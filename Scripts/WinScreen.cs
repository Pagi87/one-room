﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WinScreen : MonoBehaviour {
    [SerializeField]
    Image panel;

    [SerializeField]
    Text timeText;

    static WinScreen singleton;

    void Awake()
    {
        singleton = this;
        gameObject.SetActive(false);
    }

    public static void Win(float timeTook) {
        singleton.gameObject.SetActive(true);
        singleton.StartCoroutine(singleton.ShowPanel(timeTook));
        DeathScreen.End = true;
    }

    IEnumerator ShowPanel(float timeSurived)
    {
        Lava.Stop();
        var time = Time.time;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        timeText.text = timeSurived.ToString("F2") + " seconds";
        SortScore(timeSurived);
        while (Time.time - time < 1)
        {
            panel.color += new Color(0, 0, 0, Time.deltaTime);
            yield return null;
        }
    }

    void SortScore(float lived) {
        var t = PlayerPrefs.GetFloat("first", 0);
        if (true)
        {
            var win = PlayerPrefs.GetInt("firstWin", 0) == 1 ? true : false;
            if (!win || t > lived)
            {
                PlayerPrefs.SetFloat("first", lived);
                var sec = PlayerPrefs.GetFloat("second", 0);
                PlayerPrefs.SetInt("thirdWin", PlayerPrefs.GetInt("secondWin", 0));
                PlayerPrefs.SetInt("secondWin", PlayerPrefs.GetInt("firstWin", 0));
                PlayerPrefs.SetInt("firstWin", 1);
                PlayerPrefs.SetFloat("second", t);
                PlayerPrefs.SetFloat("third", sec);
            }
            else {
                t = PlayerPrefs.GetFloat("second", 0);
                if (true)
                {
                    win = PlayerPrefs.GetInt("secondWin", 0) == 1 ? true : false;
                    if (!win || t > lived)
                    {
                        PlayerPrefs.SetFloat("second", lived);
                        PlayerPrefs.SetInt("thirdWin", PlayerPrefs.GetInt("secondWin", 0));
                        PlayerPrefs.SetInt("secondWin", 1);
                        PlayerPrefs.SetFloat("third", t);
                    }
                    else {
                        t = PlayerPrefs.GetFloat("third", 0);
                        if (true)
                        {
                            win = PlayerPrefs.GetInt("thirdWin", 0) == 1 ? true : false;
                            if (!win || t > lived)
                            {
                                PlayerPrefs.SetFloat("third", lived);
                                PlayerPrefs.SetInt("thirdWin", 1);
                            }
                        }
                    }
                }
            }
        }
    }
}
