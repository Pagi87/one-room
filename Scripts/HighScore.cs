﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighScore : MonoBehaviour {

    [SerializeField]
    Text first;
    [SerializeField]
    Text second;
    [SerializeField]
    Text third;

    // Use this for initialization
    void Start () {
        var firstTime = PlayerPrefs.GetFloat("first", 0);
        if (firstTime != 0) {
            var win = PlayerPrefs.GetInt("firstWin",0) == 1 ? true : false;
            first.text =( win ? "Won in " : "Died after " )+ firstTime.ToString("F2") + "s";
        }

        var secondTime = PlayerPrefs.GetFloat("second", 0);
        if (secondTime != 0)
        {
            var win = PlayerPrefs.GetInt("secondWin", 0) == 1 ? true : false;
            second.text = (win ? "Won in " : "Died after ") + secondTime.ToString("F2") + "s";
        }

        var thirdTime = PlayerPrefs.GetFloat("third", 0);
        if (thirdTime != 0)
        {
            var win = PlayerPrefs.GetInt("thirdWin", 0) == 1 ? true : false;
            third.text = (win ? "Won in " : "Died after ") + thirdTime.ToString("F2") + "s";
        }
    }
	
}
