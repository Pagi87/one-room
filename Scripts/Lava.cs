﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lava : MonoBehaviour
{
    [SerializeField]
    float beforeRise;
    [SerializeField]
    float riseSpeed;

    [SerializeField]
    AudioSource sound;
    [SerializeField]
    AudioSource musicSound;

    [SerializeField]
    Transform player;
    

    [SerializeField]
    float soundVolumeRolloff;

    public static float TimeSurvived { get; private set; }

    static Lava singleton;

    bool end = false;

    void Start()
    {
        singleton = this;
        StartCoroutine(Rise());
    }

    IEnumerator Rise()
    {
        TimeSurvived = 0;
        sound.volume = 1f / Mathf.Abs(Mathf.Pow((transform.position.y - player.position.y), 2) + 1f);
        musicSound.volume = 1 - sound.volume;
        yield return new WaitForSeconds(beforeRise);
        while (true)
        {
            transform.position += new Vector3(0, riseSpeed * Time.deltaTime+Mathf.Clamp(TimeSurvived/900f,0f,0.03f)*Time.deltaTime, 0);
            TimeSurvived += Time.deltaTime;

            sound.volume = 1f / Mathf.Abs(Mathf.Pow((transform.position.y - player.position.y), 2) + 1f);
            musicSound.volume = 1 - sound.volume;
            yield return null;
        }
    }


    void OnTriggerEnter(Collider col)
    {
        var e = col.GetComponent<Entity>();
        if (e && !end)
            e.Die();
    }

    public static void Stop() {
        singleton.StopAllCoroutines();
        singleton.end = true;

        singleton.sound.volume = 0;
        singleton.musicSound.volume = 0;
    }
}
