﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tutorial : MonoBehaviour
{
    [SerializeField]
    Text left;
    [SerializeField]
    Text right;
    [SerializeField]
    Text getUp;
    [SerializeField]
    CanvasGroup group;

    void Update()
    {
        if (Input.GetMouseButtonDown(0) && left.gameObject.activeSelf)
            StartCoroutine(HideText(left));
        if (Input.GetMouseButtonDown(1) && right.gameObject.activeSelf)
            StartCoroutine(HideText(right));
        if (Lava.TimeSurvived > 0)
        {
            StartCoroutine(HideText(getUp));
            StartCoroutine(Fade());
        }
        if (Lava.TimeSurvived > 1)
            gameObject.SetActive(false);
    }

    IEnumerator HideText(Text text)
    {
        while (text.color.a > 0.1f)
        {
            text.rectTransform.anchoredPosition += Vector2.right * Time.deltaTime * Screen.width;
            text.color -= new Color(0, 0, 0, 0.05f);
            yield return null;
        }
        text.gameObject.SetActive(false);

    }

    IEnumerator Fade()
    {
        while (group.alpha > 0.05f) {
            group.alpha -= 0.05f;
            yield return null;
        }
    }
}
