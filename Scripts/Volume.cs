﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Volume : MonoBehaviour {
    [SerializeField]
    Slider volumeSlider;
    [SerializeField]
    UnityEngine.Audio.AudioMixer mixer;
	// Use this for initialization
	void Start () {
        volumeSlider.value = PlayerPrefs.GetFloat("volume", 0f);
	}

    void OnDestroy() {
        PlayerPrefs.SetFloat("volume", volumeSlider.value);
    }

    public void ChangeVolume(float f) {
        mixer.SetFloat("volume", f);
    }
}
