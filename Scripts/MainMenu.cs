﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

    void Start() {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

	public void StartGame()
    {
        SceneManager.LoadScene("Game");

    }

    public void StartFreePlay() {

    }

    public void OpenCredits() {

    }

    public void Exit() {
        Application.Quit();
    }
}
