﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    Rigidbody rigid;
    [SerializeField]
    Camera cam;

    [SerializeField]
    float jumpSpeed;
    [SerializeField]
    float jumpFalloff;
    [SerializeField]
    float movementSpeed;
    [SerializeField]
    float pullSpeed;

    [SerializeField]
    LayerMask raycast;
    public static LayerMask RayCastMask;
    [SerializeField]
    Hook hook;
    [SerializeField]
    AudioSource hookAudio;

    [SerializeField]
    AudioSource land1;
    [SerializeField]
    AudioSource land2;

    bool jumping;
    float flyTime;

    void Start()
    {
        rigid = GetComponent<Rigidbody>();
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        rigid.maxAngularVelocity = 0;
        RayCastMask = raycast;
    }

    float yRotation;
    bool pulling;
    // Update is called once per frame
    void Update()
    {
        /*
        bool grounded = Physics.Raycast(transform.position, Vector3.down, 0.04f, raycast);
        if (grounded)
        {
            velocity = transform.localRotation * new Vector3(Input.GetAxis("Horizontal"), rigid.velocity.y, Input.GetAxis("Vertical")).normalized * movementSpeed;

            //Debug.DrawRay(transform.position + new Vector3(0, 0.03f, 0), new Vector3(velocity.x, 0, velocity.z).normalized * 0.15f,Color.green,1);
            //Debug.DrawRay(transform.position + new Vector3(0, 0.1f, 0), new Vector3(velocity.x, 0, velocity.z).normalized * 0.15f,Color.grey,1);
            
            if (!jumping && Physics.Raycast(transform.position + new Vector3(0, 0.03f, 0), new Vector3(velocity.x,0, velocity.z), 0.15f, raycast) && !Physics.Raycast(transform.position + new Vector3(0, 0.1f, 0), new Vector3(velocity.x, 0, velocity.z), 0.15f, raycast))
            {
                velocity.y = 1;
            }
        }
        if (!grounded)
        {
            velocity.y = Mathf.Lerp(velocity.y, -9.81f, jumpFalloff * Time.deltaTime);
        }
        else if (Input.GetKeyDown(KeyCode.Space))
        {
            velocity.y = jumpSpeed;
            jumping = true;
        }
        else if (velocity.y < 0)
        {
            jumping = false;
            velocity.y = 0;
        }
        

        */
        if (Physics.Raycast(transform.position, Vector3.down, 0.08f, raycast)
            || Physics.Raycast(transform.position + new Vector3(0.02f, 0, 0), Vector3.down, 0.08f, raycast)
            || Physics.Raycast(transform.position + new Vector3(-0.02f, 0, 0), Vector3.down, 0.08f, raycast)
             || Physics.Raycast(transform.position + new Vector3(0, 0, 0.02f), Vector3.down, 0.08f, raycast)
             || Physics.Raycast(transform.position + new Vector3(0, 0, -0.02f), Vector3.down, 0.08f, raycast))
        {
            flyTime = 0;
        }
        else
            flyTime += Time.deltaTime;

        if (pulling)
        {
            var pullDir = hook.transform.position - transform.position;
            rigid.velocity = pullDir.sqrMagnitude > 0.05 ? pullDir.normalized * pullSpeed + new Vector3(0, flyTime * -0.981f * 2, 0) : new Vector3(0, flyTime * -0.981f, 0);
            if (!hookAudio.isPlaying)
                hookAudio.Play();
            hookAudio.pitch = 0.5f + rigid.velocity.magnitude / 10f;
        }
        else {
            rigid.velocity = Vector3.Lerp(rigid.velocity, new Vector3(0, 3 * -0.981f, 0), Time.deltaTime);
            hookAudio.Stop();
        }
        if (Input.GetMouseButtonDown(0))
        {
            //hook.gameObject.SetActive(true);
            pulling = false;
            hook.Shoot(cam.ScreenPointToRay(Input.mousePosition).direction);
        }
        if (Input.GetMouseButtonDown(1))
        {
            hook.Cancel();
            pulling = false;
        }

        yRotation += Input.GetAxis("Mouse Y");
        yRotation = Mathf.Clamp(yRotation, -89, 85);

        var xLook = Quaternion.AngleAxis(Input.GetAxis("Mouse X"), Vector3.up);
        var yLook = Quaternion.AngleAxis(yRotation, Vector3.left);
        transform.localRotation *= xLook;
        rigid.rotation = transform.localRotation;
        cam.transform.localRotation = yLook;
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.Escape))
            UnityEditor.EditorApplication.isPaused = true;
#endif
        if (Cursor.visible && !DeathScreen.End)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        if(Input.GetKeyDown(KeyCode.Escape))
            UnityEngine.SceneManagement.SceneManager.LoadScene("MainMenu");
    }

    void OnCollisionEnter(Collision col) {
        if (!col.collider.GetComponent<Lava>()) {
            if (Random.value > 0.5f)
                land1.Play();
            else land2.Play();
        }
    }

    void OnDisable() {
        hookAudio.Stop();
    }

    public void Pull()
    {
        pulling = true;
    }
}
