﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[ExecuteInEditMode]
public class LoadModel : MonoBehaviour
{
    static Dictionary<string, Mesh> meshes = new Dictionary<string, Mesh>();
    [SerializeField]
    string meshName;
    /*
    void Start()
    {
        if (string.IsNullOrEmpty(meshName))
            return;
        Mesh mesh;
        if (meshes.TryGetValue(meshName, out mesh))
        {
            GetComponent<MeshFilter>().sharedMesh = mesh;
        }
        else {
            mesh = VoxelMap.LoadMesh(meshName);
            if (mesh != null)
            {
                meshes.Add(meshName, mesh);
                GetComponent<MeshFilter>().sharedMesh = mesh;
            }
        }
        MeshCollider col = GetComponent<MeshCollider>();
        if (col) {
            col.sharedMesh = mesh;
        }
    }*/
    void OnEnable() {
        if (Application.isEditor && !Application.isPlaying)
        {

            if (string.IsNullOrEmpty(meshName))
                return;
            Mesh mesh;
            if (meshes.TryGetValue(meshName, out mesh) && mesh != null)
            {
                GetComponent<MeshFilter>().sharedMesh = mesh;
            }
            else {
                mesh = VoxelMap.LoadMesh(meshName);
                if (mesh != null)
                {
                    meshes[meshName] = mesh;
                    GetComponent<MeshFilter>().sharedMesh = mesh;
                }
            }
            MeshCollider col = GetComponent<MeshCollider>();
            if (col)
            {
                col.sharedMesh = mesh;
            }
            gameObject.name = meshName;
        }
    }
}
