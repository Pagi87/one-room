﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hook : MonoBehaviour {
    [SerializeField]
    float speed;
    [SerializeField]
    float maxFlyTime;
    Rigidbody rigid;
    LineRenderer lineRend;
    [SerializeField]
    PlayerControl player;

    [SerializeField]
    AudioSource chnapSource;

    bool engaged;
    float flyTime;

    GameObject aimedAt;

    void Awake() {
        rigid = GetComponent<Rigidbody>();
        lineRend = GetComponent<LineRenderer>();
    }

    public void Shoot(Vector3 dir) {
        gameObject.SetActive(true);
        transform.position = player.transform.position + new Vector3(0,0.15f, 0);
        rigid.velocity = dir*speed;
        transform.up = dir;
        rigid.rotation = transform.rotation;
        engaged = false;
        flyTime = Time.time;
        RaycastHit hit;
        if (Physics.Raycast(new Ray(transform.position, dir), out hit, maxFlyTime * speed, PlayerControl.RayCastMask)) {
            aimedAt = hit.collider.gameObject;
        }
    }

    public void Cancel() {
        engaged = false;
        gameObject.SetActive(false);
    }

    void Update() {
        if (!engaged && Time.time - flyTime > maxFlyTime) {
            Cancel();
        }
        else if(!engaged)
            rigid.velocity += new Vector3(0, (Time.time - flyTime) * -9.81f * Time.deltaTime, 0);
        lineRend.SetPosition(0, player.transform.position+player.transform.right * 0.2f);
        lineRend.SetPosition(1, transform.position);
    }

    void OnTriggerEnter(Collider col) {
        if (col.GetComponent<PlayerControl>() != null /*|| col.gameObject != aimedAt*/)
            return;
        if (col.CompareTag("End"))
        {
            WinScreen.Win(Lava.TimeSurvived);
            player.enabled = false;
            gameObject.SetActive(false);
        }
        rigid.velocity = Vector3.zero;
        engaged = true;
        player.Pull();
        chnapSource.Play();
    }
}
