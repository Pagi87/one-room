﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entity : MonoBehaviour
{

    public void Die()
    {
        var player = GetComponent<PlayerControl>();
        if (player != null)
        {
            player.enabled = false;
            DeathScreen.OnDeath(Lava.TimeSurvived);
        }
    }
}
