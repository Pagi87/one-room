﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DeathScreen : MonoBehaviour
{
    [SerializeField]
    Image panel;
    [SerializeField]
    Text survivedTimeText;
    [SerializeField]
    GameObject underLavaPlane;

    static DeathScreen singleton;

    public static bool End;

    void Awake()
    {
        singleton = this;
        gameObject.SetActive(false);
    }

    public static void OnDeath(float timeSurvived)
    {
        singleton.gameObject.SetActive(true);
        singleton.StartCoroutine(singleton.ShowPanel(timeSurvived));
        End = true;
    }


    IEnumerator ShowPanel(float timeSurived)
    {
        var time = Time.time;
        Lava.Stop();
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        underLavaPlane.SetActive(true);
        survivedTimeText.text = timeSurived.ToString("F2") + " seconds";
        SortScore(timeSurived);
        while (Time.time - time < 1)
        {
            panel.color += new Color(0, 0, 0, Time.deltaTime / 1.5f);
            yield return null;
        }
    }

    public void Retry()
    {
        SceneManager.LoadScene("Game");
    }

    public void ReturnToMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    void SortScore(float lived) {//Hardcoding masterrace
        var t = PlayerPrefs.GetFloat("first", 0);
        if (true) {
            var win = PlayerPrefs.GetInt("firstWin", 0) == 1 ? true : false;
            if (!win && t < lived)
            {
                PlayerPrefs.SetFloat("first", lived);
                var sec = PlayerPrefs.GetFloat("second", 0);
                PlayerPrefs.SetFloat("second", t);
                PlayerPrefs.SetFloat("third", sec);
            }
            else {
                t = PlayerPrefs.GetFloat("second", 0);
                if (true) {
                    win = PlayerPrefs.GetInt("secondWin", 0) == 1 ? true : false;
                    if (!win && t < lived)
                    {
                        PlayerPrefs.SetFloat("second", lived);
                        PlayerPrefs.SetFloat("third", t);
                    }
                    else {
                        t = PlayerPrefs.GetFloat("third", 0);
                        if (true) {
                            win = PlayerPrefs.GetInt("thirdWin", 0) == 1 ? true : false;
                            if (!win && t < lived)
                            {
                                PlayerPrefs.SetFloat("third", lived);
                            }
                        }
                    }
                }
            }
        }
    }
}
