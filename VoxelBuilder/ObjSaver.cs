﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class ObjSaver
{

    public static void SaveMeshObj(Mesh mesh, string name)
    {
        StringBuilder sb = new StringBuilder();
        var vertices = mesh.vertices;
        var normals = mesh.normals;
        var oldUv = mesh.uv;
        var uv = new Vector2[vertices.Length];
        for (int i = 0; i < oldUv.Length; i++)
        {
            uv[i] = oldUv[i];
        }
        var triangles = mesh.triangles;
        sb.AppendLine("o Mesh");
        for (int i = 0; i < vertices.Length; i++)
        {
            sb.AppendLine(string.Format("v {0} {1} {2}", vertices[i].x, vertices[i].y, vertices[i].z));
        }
        for (int i = 0; i < uv.Length; i++)
        {
            sb.AppendLine(string.Format("vt {0} {1}", uv[i].x, uv[i].y));
        }
        for (int i = 0; i < normals.Length; i++)
        {
            sb.AppendLine(string.Format("vn {0} {1} {2}", normals[i].x, normals[i].y, normals[i].z));
        }/*
        sb.Append("usemtl " + rend.sharedMaterial.name);
        sb.AppendLine();*/
        for (int i = 2; i < triangles.Length; i += 3)
        {
            sb.AppendLine(string.Format("f {0}/{0}/{0} {1}/{1}/{1} {2}/{2}/{2}", triangles[i - 2] + 1, triangles[i - 1] + 1, triangles[i] + 1));
        }

        File.WriteAllText(Application.streamingAssetsPath + "/" + name + ".obj", sb.ToString());
    }
    public static Mesh LoadMeshObj(string name)
    {

        var lines = File.ReadAllLines(Application.streamingAssetsPath + "/" + name + ".obj");
        Mesh mesh = new Mesh();
        var vertices = new List<Vector3>();
        var preNormals = new List<Vector3>();
        var preUv = new List<Vector2>();
        var triangles = new List<int>();
        List<string> triLines = new List<string>();
        for (int i = 0; i < lines.Length; i++)
        {
            var args = lines[i].Split(' ');
            switch (args[0])
            {
                case ("v"):
                    vertices.Add(new Vector3(float.Parse(args[1]), float.Parse(args[2]), float.Parse(args[3])));
                    break;
                case ("vt"):
                    preUv.Add(new Vector2(float.Parse(args[1]), float.Parse(args[2])));
                    break;
                case ("vn"):
                    preNormals.Add(new Vector3(float.Parse(args[1]), float.Parse(args[2]), float.Parse(args[3])));
                    break;
                case ("f"):
                    {
                        triLines.Add(args[1]);
                        triLines.Add(args[2]);
                        triLines.Add(args[3]);
                        break;
                    }
                default:
                    break;
            }
        }
        var normals = new Vector3[vertices.Count];
        var uv = new Vector2[vertices.Count];
        for (int i = 0; i < triLines.Count; i++)
        {
            string[] args = triLines[i].Split('/');
            triangles.Add(int.Parse(args[0]) - 1);
            uv[int.Parse(args[0]) - 1] = preUv[int.Parse(args[1]) - 1];
            normals[int.Parse(args[0]) - 1] = preNormals[int.Parse(args[2]) - 1];
        }
        mesh.vertices = vertices.ToArray();
        mesh.triangles = triangles.ToArray();
        mesh.uv = uv;
        mesh.normals = normals;
        return mesh;
    }
}
