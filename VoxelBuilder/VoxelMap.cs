﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class VoxelMap : MonoBehaviour
{
    [SerializeField]
    float voxelsPerUnit = 16;
    [SerializeField]
    MeshFilter filter;
    public float VoxelsPerUnit {
        get { return voxelsPerUnit; }
    }
    public bool useParticles;
    Dictionary<Int3, Color> blocks = new Dictionary<Int3, Color>();
    string fileName;
    [SerializeField]
    ParrticlesInGrid particles;

    public void SetBlock(Int3 pos, Color col)
    {
        blocks[pos] = col;
        if(useParticles)
            particles.AddParticle(pos,new ParticleSystem.Particle() {position = ((Vector3)pos)/voxelsPerUnit + new Vector3(0.5f/voxelsPerUnit, 0.5f / voxelsPerUnit, 0.5f / voxelsPerUnit), startSize = 1f/voxelsPerUnit, startColor = col });
        else
            filter.mesh = MeshBuilder.BuildMesh(blocks, voxelsPerUnit);
    }
    public bool HasBlock(Int3 pos, out Color col)
    {
        return blocks.TryGetValue(pos, out col);
    }
    public bool HasBlock(Int3 pos)
    {
        return blocks.ContainsKey(pos);
    }
    public void RemoveBlock(Int3 pos) {
        blocks.Remove(pos);
        if (useParticles)
            particles.RemoveParticle(pos);
        else {
            filter.mesh = MeshBuilder.BuildMesh(blocks, voxelsPerUnit);
        }
    }

    void Update() {
    }
    public void SetFileName(string s) {
        fileName = s;
    }
    public void LoadToEditor() {
        if (string.IsNullOrEmpty(fileName))
            return;
        var d = BinarySaver.Load(fileName);
        if (d == null)
        {
            Debug.Log("File doesn't exist");
            return;
        }
        voxelsPerUnit = d.voxelsPerUnit;
        blocks = new Dictionary<Int3, Color>();
        for (int i = 0; i < d.data.Length; i++)
        {
            blocks.Add(d.data[i].pos, new Color(d.data[i].r, d.data[i].g, d.data[i].b, d.data[i].a));
        }
        if(!useParticles)
            filter.mesh = MeshBuilder.BuildMesh(blocks, voxelsPerUnit);
    }
    public void SaveFromEditor() {
        if (string.IsNullOrEmpty(fileName) || blocks.Count == 0)
            return;
        BinarySaver.Save(blocks, voxelsPerUnit, fileName);
    }
    public static Mesh LoadMesh(string s) {
        if (string.IsNullOrEmpty(s))
        {
            Debug.LogError("File name cannot be empty.");
            return null;
        }
        var d = BinarySaver.Load(s);
        if (d == null)
        {
            Debug.Log("File doesn't exist");
            return null;
        }
        var blocks = new Dictionary<Int3, Color>();
        for (int i = 0; i < d.data.Length; i++)
        {
            blocks.Add(d.data[i].pos, new Color(d.data[i].r, d.data[i].g, d.data[i].b, d.data[i].a));
        }
        return MeshBuilder.BuildMesh(blocks, d.voxelsPerUnit);
    }

    public void SetVoxelsPerUnit(string s) {
        float f;
        if (float.TryParse(s, out f))
        {
            voxelsPerUnit = f;

        }
        else Debug.LogWarning("Wrong value.");
    }
}
