﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class BinarySaver {
    [System.Serializable]
    public class PosAndCol {
        public Int3 pos;
        public float r, g, b, a;
    }

    [System.Serializable]
    public class Data {
        public PosAndCol[] data;
        public float voxelsPerUnit;
    }

    public static void Save(Dictionary<Int3, Color> map,float voxelsPerUnit, string name) {
        var e = map.GetEnumerator();
        PosAndCol[] data = new PosAndCol[map.Keys.Count];
        int i = 0;
        while (e.MoveNext()) {
            data[i] = new PosAndCol();
            data[i].pos = e.Current.Key;
            data[i].r = e.Current.Value.r;
            data[i].g = e.Current.Value.g;
            data[i].b = e.Current.Value.b;
            data[i].a = e.Current.Value.a;

            i++;
        }
        var save = new Data() { data = data, voxelsPerUnit = voxelsPerUnit };
        BinaryFormatter bf = new BinaryFormatter();
        Directory.CreateDirectory(Application.streamingAssetsPath);
        FileStream file = File.Create(Application.streamingAssetsPath + "\\" + name + ".vox");
        bf.Serialize(file, save);
        file.Close();
    }

    public static Data Load(string name) {
        if (!File.Exists(Application.streamingAssetsPath + "\\" + name + ".vox"))
            return null;

        Data save;
        FileStream file = null;
        try
        {
            BinaryFormatter bf = new BinaryFormatter();
            file = File.Open(Application.streamingAssetsPath + "\\" + name + ".vox", FileMode.Open);
            save = (Data)bf.Deserialize(file);
            file.Close();
        }
        finally {
            if(file != null)
                file.Close();
        }
        return save;
    }
}
