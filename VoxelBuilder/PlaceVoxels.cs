﻿using UnityEngine;
using System.Collections;

public class PlaceVoxels : MonoBehaviour
{
    [SerializeField]
    VoxelMap map;
    Camera cam;

    public static bool hascontrol = true;

    void Start()
    {
        cam = GetComponent<Camera>();
        helpBlock.transform.localScale = new Vector3(1f / map.VoxelsPerUnit, 1f / map.VoxelsPerUnit, 1f / map.VoxelsPerUnit);
    }
    float timer = 0;
    void Update()
    {
        timer += Time.deltaTime;
        if (!hascontrol)
            return;
        if (Input.GetMouseButtonDown(0) || (Input.GetMouseButton(0) && timer > 0.1f))
        {
            Place();
            timer = 0;
        }
        else if (Input.GetMouseButtonDown(1) || (Input.GetMouseButton(1) && timer > 0.1f))
        {
            Remove();
            timer = 0;
        }
        else if (Input.GetKey(KeyCode.F))
            Paint();
        else if (Input.GetKeyDown(KeyCode.P))
            Pick();
        ShowBlock();

    }

    void Place()
    {
        var ray = cam.ScreenPointToRay(Input.mousePosition);

        var start = ray.origin * map.VoxelsPerUnit;
        var end = (ray.origin + ray.direction * 40) * map.VoxelsPerUnit;
        var dX = end.x - start.x;
        var dY = end.y - start.y;
        var dZ = end.z - start.z;
        var num = Mathf.Abs(dX) > Mathf.Abs(dY) ? Mathf.Abs(dX) > Mathf.Abs(dZ) ? Mathf.Abs(dX) : Mathf.Abs(dZ) : Mathf.Abs(dY) > Mathf.Abs(dZ) ? Mathf.Abs(dY) : Mathf.Abs(dZ);
        Vector3 currentPos = start;
        Vector3 increment = new Vector3(dX / num, dY / num, dZ / num) / 16f;
        for (int i = 0; i < num * 16; i += 1)
        {
            if (currentPos.y < 0 || map.HasBlock(Int3.Floor(currentPos)))
            {
                if (Int3.Floor(currentPos - increment).y < 0)
                    continue;
                map.SetBlock(Int3.Floor(currentPos - increment), ColorPanel.CurrentColor);
                break;
            }
            currentPos += increment;
        }
    }
    void Paint()
    {
        var ray = cam.ScreenPointToRay(Input.mousePosition);

        var start = ray.origin * map.VoxelsPerUnit;
        var end = (ray.origin + ray.direction * 40) * map.VoxelsPerUnit;
        var dX = end.x - start.x;
        var dY = end.y - start.y;
        var dZ = end.z - start.z;
        var num = Mathf.Abs(dX) > Mathf.Abs(dY) ? Mathf.Abs(dX) > Mathf.Abs(dZ) ? Mathf.Abs(dX) : Mathf.Abs(dZ) : Mathf.Abs(dY) > Mathf.Abs(dZ) ? Mathf.Abs(dY) : Mathf.Abs(dZ);
        Vector3 currentPos = start;
        Vector3 increment = new Vector3(dX / num, dY / num, dZ / num) / 16f;
        for (int i = 0; i < num * 16; i += 1)
        {
            if (map.HasBlock(Int3.Floor(currentPos)))
            {
                map.SetBlock(Int3.Floor(currentPos), ColorPanel.CurrentColor);
                break;
            }
            currentPos += increment;
        }
    }

    [SerializeField]
    ColorPanel colorPanel;

    void Pick()
    {
        var ray = cam.ScreenPointToRay(Input.mousePosition);

        var start = ray.origin * map.VoxelsPerUnit;
        var end = (ray.origin + ray.direction * 40) * map.VoxelsPerUnit;
        var dX = end.x - start.x;
        var dY = end.y - start.y;
        var dZ = end.z - start.z;
        var num = Mathf.Abs(dX) > Mathf.Abs(dY) ? Mathf.Abs(dX) > Mathf.Abs(dZ) ? Mathf.Abs(dX) : Mathf.Abs(dZ) : Mathf.Abs(dY) > Mathf.Abs(dZ) ? Mathf.Abs(dY) : Mathf.Abs(dZ);
        Vector3 currentPos = start;
        Vector3 increment = new Vector3(dX / num, dY / num, dZ / num) / 16f;
        for (int i = 0; i < num * 16; i += 1)
        {
            if (map.HasBlock(Int3.Floor(currentPos)))
            {
                Color col;
                map.HasBlock(Int3.Floor(currentPos), out col);
                colorPanel.SetColor(col);
                break;
            }
            currentPos += increment;
        }
    }

    void Remove()
    {
        var ray = cam.ScreenPointToRay(Input.mousePosition);

        var start = ray.origin * map.VoxelsPerUnit;
        var end = (ray.origin + ray.direction * 40) * map.VoxelsPerUnit;
        var dX = end.x - start.x;
        var dY = end.y - start.y;
        var dZ = end.z - start.z;
        var num = Mathf.Abs(dX) > Mathf.Abs(dY) ? Mathf.Abs(dX) > Mathf.Abs(dZ) ? Mathf.Abs(dX) : Mathf.Abs(dZ) : Mathf.Abs(dY) > Mathf.Abs(dZ) ? Mathf.Abs(dY) : Mathf.Abs(dZ);
        Vector3 currentPos = start;
        Vector3 increment = new Vector3(dX / num, dY / num, dZ / num) / 16f;
        for (int i = 0; i < num * 16; i += 1)
        {
            if (map.HasBlock(Int3.Floor(currentPos)))
            {
                map.RemoveBlock(Int3.Floor(currentPos));
                break;
            }
            currentPos += increment;
        }
    }
    [SerializeField]
    Material helpMat;
    [SerializeField]
    GameObject helpBlock;

    void ShowBlock()
    {
        helpBlock.transform.localScale = new Vector3(1f / map.VoxelsPerUnit, 1f / map.VoxelsPerUnit, 1f / map.VoxelsPerUnit);
        var ray = cam.ScreenPointToRay(Input.mousePosition);

        var start = ray.origin * map.VoxelsPerUnit;
        var end = (ray.origin + ray.direction * 40) * map.VoxelsPerUnit;
        var dX = end.x - start.x;
        var dY = end.y - start.y;
        var dZ = end.z - start.z;
        var num = Mathf.Abs(dX) > Mathf.Abs(dY) ? Mathf.Abs(dX) > Mathf.Abs(dZ) ? Mathf.Abs(dX) : Mathf.Abs(dZ) : Mathf.Abs(dY) > Mathf.Abs(dZ) ? Mathf.Abs(dY) : Mathf.Abs(dZ);
        Vector3 currentPos = start;
        Vector3 increment = new Vector3(dX / num, dY / num, dZ / num) / 16f;
        for (int i = 0; i < num * 16; i += 1)
        {
            if (currentPos.y < 0 || map.HasBlock(Int3.Floor(currentPos)))
            {
                helpBlock.transform.position = (Vector3)Int3.Floor(currentPos - increment) / map.VoxelsPerUnit + new Vector3(0.5f / map.VoxelsPerUnit, 0.5f / map.VoxelsPerUnit, 0.5f / map.VoxelsPerUnit);
                break;
            }
            currentPos += increment;
        }
    }
}
