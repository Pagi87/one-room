﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorPanel : MonoBehaviour {
    public static Color CurrentColor = Color.white;
    public Image image;
    public Slider[] sliders;
    public void SetR(float f) {
        CurrentColor.r = f;
        image.color = CurrentColor;
    }
    public void SetG(float f)
    {
        CurrentColor.g = f;
        image.color = CurrentColor;
    }
    public void SetB(float f)
    {
        CurrentColor.b = f;
        image.color = CurrentColor;
    }
    public void SetA(float f)
    {
        CurrentColor.a = f;
        image.color = CurrentColor;
    }

    public void SetColor(Color col) {
        CurrentColor = col;
        sliders[0].value = col.r;
        sliders[1].value = col.g;
        sliders[2].value = col.b;
        sliders[3].value = col.a;
        image.color = CurrentColor;
    }
}
