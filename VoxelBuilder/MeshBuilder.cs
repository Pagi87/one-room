﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MeshBuilder
{

    public static Mesh BuildMesh(Dictionary<Int3, Color> map, float voxelsPerUnit)
    {
        if (map.Count == 0)
            return null;
        var voxelSize = 1f / voxelsPerUnit;
        var e = map.Keys.GetEnumerator();
        int startX, startZ, endX, endY, endZ;
        startX = 9999;
        startZ = 9999;
        endX = -9999;
        endY = 0;
        endZ = -9999;
        while (e.MoveNext())
        {
            if (e.Current.x < startX)
                startX = e.Current.x;
            if (e.Current.x > endX)
                endX = e.Current.x;
            if (e.Current.z < startZ)
                startZ = e.Current.z;
            if (e.Current.z > endZ)
                endZ = e.Current.z;
            if (e.Current.y > endY)
                endY = e.Current.y;
        }
        Int3 offset = new Int3(startX, 0,startZ);
        bool[,,] maskArray = new bool[endX - startX + 1, endY + 1, endZ - startZ + 1];
        for (int x = 0; x < maskArray.GetLength(0); x++)
        {
            for (int y = 0; y < maskArray.GetLength(1); y++)
            {
                for (int z = 0; z < maskArray.GetLength(2); z++)
                {
                    maskArray[x, y, z] = map.ContainsKey(new Int3(x,y, z)+offset);
                }
            }
        }

        Mesh mesh = new Mesh();
        var vertices = new List<Vector3>(map.Keys.Count * 8);
        var colors = new List<Color>(map.Keys.Count * 8);
        var tris = new List<int>(map.Keys.Count * 6 * 2 * 3);

        for (int x = 0; x < maskArray.GetLength(0); x++)
        {
            for (int y = 0; y < maskArray.GetLength(1); y++)
            {
                for (int z = 0; z < maskArray.GetLength(2); z++)
                {
                    if (maskArray[x, y, z]) {
                        var pos = new Int3(x, y, z) + offset;
                        var col = map[pos];

                        if (x== 0 || !maskArray[x - 1, y, z]) {
                            vertices.Add(pos);
                            vertices.Add(pos + Int3.forward);
                            vertices.Add(pos + Int3.up);
                            vertices.Add(pos + Int3.forward + Int3.up);

                            colors.Add(col);
                            colors.Add(col);
                            colors.Add(col);
                            colors.Add(col);

                            tris.Add(vertices.Count - 4);
                            tris.Add(vertices.Count - 3);
                            tris.Add(vertices.Count - 2);

                            tris.Add(vertices.Count - 3);
                            tris.Add(vertices.Count - 1);
                            tris.Add(vertices.Count - 2);
                        }

                        if (x == maskArray.GetLength(0) - 1 || !maskArray[x + 1, y, z]) {
                            vertices.Add(pos + Int3.right);
                            vertices.Add(pos + Int3.forward + Int3.right);
                            vertices.Add(pos + Int3.up + Int3.right);
                            vertices.Add(pos + Int3.forward + Int3.up + Int3.right);

                            colors.Add(col);
                            colors.Add(col);
                            colors.Add(col);
                            colors.Add(col);

                            tris.Add(vertices.Count - 3);
                            tris.Add(vertices.Count - 4);
                            tris.Add(vertices.Count - 2);

                            tris.Add(vertices.Count - 1);
                            tris.Add(vertices.Count - 3);
                            tris.Add(vertices.Count - 2);
                        }

                        if (y == 0 || !maskArray[x, y-1, z])
                        {
                            vertices.Add(pos);
                            vertices.Add(pos + Int3.right);
                            vertices.Add(pos + Int3.forward);
                            vertices.Add(pos + Int3.forward + Int3.right);

                            colors.Add(col);
                            colors.Add(col);
                            colors.Add(col);
                            colors.Add(col);

                            tris.Add(vertices.Count - 4);
                            tris.Add(vertices.Count - 3);
                            tris.Add(vertices.Count - 2);

                            tris.Add(vertices.Count - 3);
                            tris.Add(vertices.Count - 1);
                            tris.Add(vertices.Count - 2);
                        }

                        if (y == maskArray.GetLength(1) - 1 || !maskArray[x, y+1, z])
                        {
                            vertices.Add(pos + Int3.up);
                            vertices.Add(pos + Int3.right + Int3.up);
                            vertices.Add(pos + Int3.forward + Int3.up);
                            vertices.Add(pos + Int3.forward + Int3.right + Int3.up);

                            colors.Add(col);
                            colors.Add(col);
                            colors.Add(col);
                            colors.Add(col);

                            tris.Add(vertices.Count - 3);
                            tris.Add(vertices.Count - 4);
                            tris.Add(vertices.Count - 2);

                            tris.Add(vertices.Count - 1);
                            tris.Add(vertices.Count - 3);
                            tris.Add(vertices.Count - 2);
                        }

                        if (z == 0 || !maskArray[x, y, z-1])
                        {
                            vertices.Add(pos);
                            vertices.Add(pos + Int3.up);
                            vertices.Add(pos + Int3.right);
                            vertices.Add(pos + Int3.up + Int3.right);

                            colors.Add(col);
                            colors.Add(col);
                            colors.Add(col);
                            colors.Add(col);

                            tris.Add(vertices.Count - 4);
                            tris.Add(vertices.Count - 3);
                            tris.Add(vertices.Count - 2);

                            tris.Add(vertices.Count - 3);
                            tris.Add(vertices.Count - 1);
                            tris.Add(vertices.Count - 2);
                        }

                        if (z == maskArray.GetLength(2) - 1 || !maskArray[x, y, z+1])
                        {
                            vertices.Add(pos + Int3.forward);
                            vertices.Add(pos + Int3.up + Int3.forward);
                            vertices.Add(pos + Int3.right + Int3.forward);
                            vertices.Add(pos + Int3.up + Int3.right + Int3.forward);

                            colors.Add(col);
                            colors.Add(col);
                            colors.Add(col);
                            colors.Add(col);

                            tris.Add(vertices.Count - 3);
                            tris.Add(vertices.Count - 4);
                            tris.Add(vertices.Count - 2);

                            tris.Add(vertices.Count - 1);
                            tris.Add(vertices.Count - 3);
                            tris.Add(vertices.Count - 2);
                        }
                    }
                }
            }
        }
        for (int i = 0; i < vertices.Count; i++)
        {
            vertices[i] /= voxelsPerUnit;
        }
        mesh.SetVertices(vertices);
        mesh.SetTriangles(tris, 0);
        mesh.SetColors(colors);
        mesh.RecalculateNormals();
        return mesh;
    }
}
