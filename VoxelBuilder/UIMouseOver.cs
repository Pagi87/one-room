﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;
using System;

public class UIMouseOver : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public void OnPointerEnter(PointerEventData eventData)
    {
        PlaceVoxels.hascontrol = false;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        PlaceVoxels.hascontrol = true;
    }
}
