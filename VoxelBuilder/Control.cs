﻿using UnityEngine;
using System.Collections;

public class Control : MonoBehaviour
{
    [SerializeField]
    Material helpMat;

    Vector3 cameraForward;
    Camera cam;
    Vector3 oldMousePos;
    public Vector3 cameraPivot = Vector3.zero;
    void Start()
    {
        cam = GetComponent<Camera>();
    }

    void Update()
    {
        cameraForward = transform.forward;
        if (Input.mouseScrollDelta.y != 0) Scroll();
        else if (Input.GetKey(KeyCode.LeftShift) && Input.GetMouseButtonDown(2)) StartShiftPivot();
        else if (Input.GetKey(KeyCode.LeftShift) && Input.GetMouseButton(2)) ShiftPivot();
        else if (Input.GetMouseButton(2)) RotateView();
        else if (Input.GetKeyDown(KeyCode.Alpha1) || Input.GetKeyDown(KeyCode.Keypad1)) LookFront(Input.GetKey(KeyCode.LeftControl));
        else if (Input.GetKeyDown(KeyCode.Alpha3) || Input.GetKeyDown(KeyCode.Keypad3)) LookSide(Input.GetKey(KeyCode.LeftControl));
        else if (Input.GetKeyDown(KeyCode.Alpha7) || Input.GetKeyDown(KeyCode.Keypad7)) LookTop(Input.GetKey(KeyCode.LeftControl));
        else if (Input.GetKeyDown(KeyCode.Alpha5) || Input.GetKeyDown(KeyCode.Keypad5)) SwitchCameraModes();
    }

    void LateUpdate()
    {
        oldMousePos = Input.mousePosition;
    }

    void OnPostRender()
    {
        DrawHelpLines();
        //DrawPivot();
    }
   

    void DrawPivot()
    {
        GL.Begin(GL.QUADS);
        helpMat.SetPass(0);
        GL.Vertex(cameraPivot + new Vector3(-0.1f, 0, -0.1f));
        GL.Vertex(cameraPivot + new Vector3(-0.1f, 0, 0.1f));
        GL.Vertex(cameraPivot + new Vector3(0.1f, 0, 0.1f));
        GL.Vertex(cameraPivot + new Vector3(0.1f, 0, -0.1f));

        GL.Vertex(cameraPivot + new Vector3(0.1f, 0, -0.1f));
        GL.Vertex(cameraPivot + new Vector3(0.1f, 0, 0.1f));
        GL.Vertex(cameraPivot + new Vector3(-0.1f, 0, 0.1f));
        GL.Vertex(cameraPivot + new Vector3(-0.1f, 0, -0.1f));

        GL.End();
    }
    Vector3 mouseProjection;
    void StartShiftPivot()
    {
        var ray = cam.ScreenPointToRay(Input.mousePosition);
        var d = -(cameraForward.x * cameraPivot.x + cameraForward.y * cameraPivot.y + cameraForward.z * cameraPivot.z);
        var t = -(d + ray.origin.x * cameraForward.x + ray.origin.y * cameraForward.y + ray.origin.z * cameraForward.z)
                /
            (cameraForward.x * ray.direction.x + cameraForward.y * ray.direction.y + cameraForward.z * ray.direction.z);
        mouseProjection = new Vector3(ray.origin.x + t * ray.direction.x, ray.origin.y + t * ray.direction.y, ray.origin.z + t * ray.direction.z);
    }
    void ShiftPivot()
    {

        var ray = cam.ScreenPointToRay(Input.mousePosition);
        var d = -(cameraForward.x * cameraPivot.x + cameraForward.y * cameraPivot.y + cameraForward.z * cameraPivot.z);
        var t = -(d + ray.origin.x * cameraForward.x + ray.origin.y * cameraForward.y + ray.origin.z * cameraForward.z)
                /
            (cameraForward.x * ray.direction.x + cameraForward.y * ray.direction.y + cameraForward.z * ray.direction.z);
        var newPos = new Vector3(ray.origin.x + t * ray.direction.x, ray.origin.y + t * ray.direction.y, ray.origin.z + t * ray.direction.z);
        cameraPivot = cameraPivot - (newPos - mouseProjection) / 2;
        transform.position = transform.position - (newPos - mouseProjection) / 2;
        mouseProjection = newPos;
    }

    void SwitchCameraModes()
    {
        if (cam.orthographic)
        {
            transform.position = -cam.orthographicSize * (transform.forward) + cameraPivot;
        }
        else {
            cam.orthographicSize = (transform.position - cameraPivot).magnitude;
            cam.transform.position = -20 * cam.transform.forward;
        }
        cam.orthographic = !cam.orthographic;
    }

    void LookFront(bool opposite)
    {
        if (opposite)
        {
            var dist = (transform.position - cameraPivot).magnitude;
            transform.rotation = Quaternion.Euler(0, 180, 0);
            transform.position = new Vector3(cameraPivot.x, cameraPivot.y, cameraPivot.z + dist);
        }
        else {
            var dist = (transform.position - cameraPivot).magnitude;
            transform.rotation = Quaternion.Euler(0, 0, 0);
            transform.position = new Vector3(cameraPivot.x, cameraPivot.y, cameraPivot.z - dist);
        }
    }
    void LookSide(bool opposite)
    {
        if (opposite)
        {
            var dist = (transform.position - cameraPivot).magnitude;
            transform.rotation = Quaternion.Euler(0, 90, 0);
            transform.position = new Vector3(cameraPivot.x - dist, cameraPivot.y, cameraPivot.z);
        }
        else {
            var dist = (transform.position - cameraPivot).magnitude;
            transform.rotation = Quaternion.Euler(0, -90, 0);
            transform.position = new Vector3(cameraPivot.x + dist, cameraPivot.y, cameraPivot.z);
        }
    }
    void LookTop(bool opposite)
    {
        if (opposite)
        {
            var dist = (transform.position - cameraPivot).magnitude;
            transform.rotation = Quaternion.Euler(-90, 0, 0);
            transform.position = new Vector3(cameraPivot.x, cameraPivot.y - dist, cameraPivot.z);
        }
        else {
            var dist = (transform.position - cameraPivot).magnitude;
            transform.rotation = Quaternion.Euler(90, 0, 0);
            transform.position = new Vector3(cameraPivot.x, cameraPivot.y + dist, cameraPivot.z);
        }
    }

    void DrawHelpLines()
    {
        if ((transform.position).sqrMagnitude > 22500) return;
        GL.Begin(GL.LINES);
        helpMat.SetPass(0);/*
        GL.Vertex(new Vector3(-5,0,0));
        GL.Vertex(new Vector3(5, 0, 0));

        GL.Vertex(new Vector3(0, 0, -5));
        GL.Vertex(new Vector3(0, 0, 5));*/
        
        for (int i = -10; i < 10; i++)
        {
            GL.Vertex(new Vector3(-10, 0, i+0.5f));
            GL.Vertex(new Vector3(10, 0, i + 0.5f));


            GL.Vertex(new Vector3(i + 0.5f, 0, -10));
            GL.Vertex(new Vector3(i + 0.5f, 0, 10));
        }

        GL.End();
    }

    void Scroll()
    {
        float amount = Input.GetKey(KeyCode.LeftShift) ? 2f : 0.125f;
        if (cam.orthographic)
        {
            cam.orthographicSize += -Input.mouseScrollDelta.y * amount;
            if (cam.orthographicSize <= 0.1f) cam.orthographicSize = 0.11f;
        }
        else {
            transform.position += transform.forward * Input.mouseScrollDelta.y * amount;
        }
    }
    void RotateView()
    {
        var rot = (Input.mousePosition - oldMousePos);/*
        rot.x /= Screen.width;
        rot.y /= Screen.height;*/
        transform.RotateAround(cameraPivot, Vector3.up, rot.x); // old transform.rotation*new Vector3(rot.y, -rot.x, 0)
        transform.RotateAround(cameraPivot, -transform.right, rot.y);
    }

}
