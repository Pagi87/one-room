﻿Shader "LavaShader" {
	Properties{
		_Color("Color", Color) = (1,1,1,1)
		_MainTex("Albedo (RGB)", 2D) = "white" {}
	_Glossiness("Smoothness", Range(0,1)) = 0.5
		_Metallic("Metallic", Range(0,1)) = 0.0
	}
		SubShader{
		Tags{ "RenderType" = "Opaque" }
		LOD 200

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
#pragma surface surf Standard vertex:vert fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
#pragma target 3.0

		sampler2D _MainTex;

	struct Input {
		float2 uv_MainTex;
		float4 vertexColor;
	};

	half _Glossiness;
	half _Metallic;
	fixed4 _Color;

	float rand(float f)
	{
		return frac(sin(dot(f, 12.9898)) * 43758.5453);
	}

	void vert(inout appdata_full v, out Input o)
	{
		UNITY_INITIALIZE_OUTPUT(Input, o);
		o.vertexColor = fixed4(0.6 + lerp(-0.3,0.4,max(0.3, rand(_Time.x / 100000 * v.vertex.z + v.vertex.x))), 1, 1, 1);
		v.vertex.y += sin(_Time.y*v.vertex.x + v.vertex.z)*0.02f + 0.02;
		v.normal.xz += fixed2(cos(_Time.y*v.vertex.x + v.vertex.z) / 2, cos(_Time.y*v.vertex.x + v.vertex.z) / 2);
	}



	void surf(Input IN, inout SurfaceOutputStandard o) {
		// Albedo comes from a texture tinted by color
		fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
		o.Albedo = c.rgb;
		if (IN.vertexColor.r > 0.61)
			o.Albedo *= IN.vertexColor.rgb * IN.vertexColor.rgb*2;/*
		else
			o.Albedo -= fixed3(0.3, 0, 0);*/
		o.Emission = IN.vertexColor.r / 6;
		// Metallic and smoothness come from slider variables
		o.Metallic = _Metallic;
		o.Smoothness = _Glossiness;
		o.Alpha = IN.vertexColor.a;
	}
	ENDCG
	}
		FallBack "Diffuse"
}
